from common.json import ModelEncoder
from .models import Conference, Location, State


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class StateListEncoder(ModelEncoder):
    model = State
    properties = ["abbreviation"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    #encoders = {
    #    "location": LocationListEncoder(),
    #}
    def get_extra_data(self, o):
        return { "location": o.location.name }


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]
    #encoders = {
    #     "state": StateListEncoder(),
    # }
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }
