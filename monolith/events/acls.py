from common.api_keys import pexels_api_key
import requests

def get_photo(city, state):
    # create a dictionary for the headers to use in the request
    headers = {"Authorization": pexels_api_key}
    # create the url for the request with the city and state
    req_url = f"https://api.pexels.com/v1/search?query={city} {state}"
    # make the request
    json_url = requests.get(req_url, headers=headers)
    print(json_url)
    # parse the json response
    picture_url = json_url.json()["photos"][0]["src"]["original"]
    print(picture_url)
    # return a dictionary that contains a picture url key and
    # one of the URLs for one of the pictures in the response
    return {"picture_url": picture_url}
