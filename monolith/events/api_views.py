from django.http import JsonResponse
from .models import Conference, Location, State
from .encoders import (ConferenceListEncoder, LocationListEncoder,
                           ConferenceDetailEncoder, LocationDetailEncoder)
from django.views.decorators.http import require_http_methods
from .acls import get_photo
import json


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False,
        )
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            content.update({"location": Location.objects.get(id=content["location"])})
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "location" in content:
            try:
                content.update({"location": Location.objects.get(id=content["location"])})
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location abbreviation"},
                    status=400,
                )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body) #decode JSON into a dict
        try:
            content.update({"state": State.objects.get(abbreviation=content["state"])}) #convert state value into a State object and put on content dict
        except State.DoesNotExist: #confirm State object is valid/exists
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        url = get_photo(content["city"], content["state"].name) #get the photo URL via acls.py (anti-corruption layer)
        content.update(**url) #add url to content dict
        print(content)
        location = Location.objects.create(**content) #create Location object and save to variable that can be passed to JsonResponse
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET","PUT","DELETE"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "state" in content:
            try:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
            except State.DoesNotExist:
                print("except ran even though no state")
                return JsonResponse(
                    {"message": "Invalid state abbreviation"},
                    status=400,
                )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
