from django.http import JsonResponse
from .models import Presentation, Status
from events.models import Conference
from .encoders import PresentationListEncoder, PresentationDetailEncoder
from django.views.decorators.http import require_http_methods
import json, pika

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            presentations,
            encoder=PresentationListEncoder,
            safe=False,
            )
    else:
        content = json.loads(request.body)
        content.update({"conference": Conference.objects.get(id=conference_id),})
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
            count, _ = Presentation.objects.filter(id=id).delete()
            return JsonResponse({ "deleted": count > 0 })
    else:
        content = json.loads(request.body)
        if "status" in content:
            try:
                content.update({"status": Status.objects.get(name=content["status"])})
            except Status.DoesNotExist:
                return JsonResponse(
                {"message": "Invalid status name"},
                status=400,
            )
        if "conference" in content:
            try:
                content.update({"conference": Conference.objects.get(name=content["conference"])})
            except Conference.DoesNotExist:
                return JsonResponse(
                {"message": "Invalid conference name"},
                status=400,
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


def send_message(queue_name, message):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name, durable=True)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message,
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    message = json.dumps({
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    })
    send_message("presentation_approvals", message)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    message = json.dumps({
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    })
    send_message("presentation_rejections", message)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
