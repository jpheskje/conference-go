import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals", durable=True)
channel.queue_declare(queue="presentation_rejections", durable=True)


def approve(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    email = content["presenter_email"]
    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    #ch.basic_ack(delivery_tag=method.delivery_tag)


def reject(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    email = content["presenter_email"]
    send_mail(
        "Your presentation has been rejected",
        f"We're sorry, {name}, but your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("donezo")
    #ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=approve,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=reject,
    auto_ack=True,
)
channel.start_consuming()
